# StarVerseCraft

## 星之幻域系列插件，自定義製作


## 作者: StarVerseDev Team


## 此插件的特色
- 可自定義大量製作方式，工作台，熔爐，高爐，鍋釜...
- 可以同製作方式多個產品，此外產品可以加權，可調整產品出產的機率
