package dev.twinklestar03.starverse.starversecraft.recipes;

import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.StonecuttingRecipe;

public class RecipeStonecutting extends StarVerseRecipe {

    public RecipeStonecutting(RecipeType recipeType, String id) {
        super(recipeType, id);
    }

    public Recipe toBukkitRecipe() {
        System.out.println(this.ingredients.get(0).getType().toString());
        StonecuttingRecipe stonecuttingRecipe = new StonecuttingRecipe(
            this.bukkitId,
            this.resultItems.get(0),
            this.ingredients.get(0).getType()
        );


        return stonecuttingRecipe;
    }
}


