package dev.twinklestar03.starverse.starversecraft.recipes;

import org.bukkit.inventory.CampfireRecipe;
import org.bukkit.inventory.Recipe;

public class RecipeCampfire extends StarVerseRecipe {

    public RecipeCampfire(RecipeType recipeType, String id, float exp, int time) {
        super(recipeType, id, exp, time);
    }

    public Recipe toBukkitRecipe() {
        CampfireRecipe recipeCamfire = new CampfireRecipe(
            this.bukkitId,
            this.resultItems.get(0),
            this.ingredients.get(0).getType(),
            this.experience,
            this.cooktime
        );

        return recipeCamfire;
    }
}

