package dev.twinklestar03.starverse.starversecraft.events;

import dev.twinklestar03.starverse.starversecraft.recipes.RecipeManager;
import dev.twinklestar03.starverse.starversecraft.recipes.StarVerseRecipe;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.*;

import static org.bukkit.Bukkit.getServer;

public class PreprocessEvents implements Listener {

    ConsoleCommandSender console = getServer().getConsoleSender();

    @EventHandler
    public void PrepareItemCraftEvent(PrepareItemCraftEvent event) {
        if (event.getInventory().getResult() != null) {
            if (event.getRecipe() != null) {

                Recipe bukkitRecipe = event.getRecipe();
                StarVerseRecipe svRecipe = RecipeManager.bukkitRecipeToSVRecipe(bukkitRecipe);
                List<ItemStack> inventory = Arrays.asList(event.getInventory().getMatrix());
                if (svRecipe != null) {
                    List<ItemStack> ingredients = svRecipe.getIngredients();

                    for (int i = 0; i < ingredients.size(); i++) {
                        if (ingredients.get(i) != null && inventory.get(i) != null) {
                            if (Objects.equals(ingredients.get(i).getItemMeta(), inventory.get(i).getItemMeta())) {
                                continue;
                            } else {
                                event.getInventory().setResult(new ItemStack(Material.AIR));
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
