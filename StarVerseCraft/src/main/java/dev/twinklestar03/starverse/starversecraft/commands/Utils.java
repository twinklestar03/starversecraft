package dev.twinklestar03.starverse.starversecraft.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;


public class Utils {
    public static TextComponent makeItemComponent(ItemStack item) {
        TextComponent itemDisplay;
        if (item.hasItemMeta()) {
            itemDisplay = new TextComponent(ChatColor.RESET + "[" + item.getItemMeta().getDisplayName() + ChatColor.RESET + "]");
        } else {
            itemDisplay = new TextComponent(ChatColor.RESET + "[" + item.getType().toString() + ChatColor.RESET + "]");
        }

        itemDisplay.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, new BaseComponent[]{new TextComponent(
            CraftItemStack.asNMSCopy(item).save(new NBTTagCompound()).toString())}));

        return itemDisplay;
    }
}
