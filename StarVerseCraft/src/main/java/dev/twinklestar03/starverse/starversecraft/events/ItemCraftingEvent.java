package dev.twinklestar03.starverse.starversecraft.events;

import java.util.*;

import dev.twinklestar03.starverse.starversecraft.recipes.RecipeManager;
import dev.twinklestar03.starverse.starversecraft.recipes.StarVerseRecipe;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockCookEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.inventory.ItemStack;

public class ItemCraftingEvent implements Listener{
    /**
     * Return a ItemStack using weighted random.
     * 
     * @return ItemStack
     */
    private ItemStack getWeightedRandomItemStack(Map<ItemStack, Double> weights) {
        // From StackOverflow: https://stackoverflow.com/questions/6737283/weighted-randomness-in-java
        Random random = new Random();
        ItemStack result = null;
        double bestValue = Double.MAX_VALUE;
    
        for (ItemStack element : weights.keySet()) {
            double value = -Math.log(random.nextDouble()) / weights.get(element);
    
            if (value < bestValue) {
                bestValue = value;
                result = element;
            }
        }
    
        return result;
    }
    @EventHandler
    public void onCraftItem(CraftItemEvent e){
        Map<ItemStack, Double> map = new HashMap<>();
        List<ItemStack> key = Objects.requireNonNull(RecipeManager.bukkitRecipeToSVRecipe(e.getRecipe())).getResultItems();
        List<Double> weight = Objects.requireNonNull(RecipeManager.bukkitRecipeToSVRecipe(e.getRecipe())).getWeight();
        for (int i=0; i<key.size(); i++) {
            map.put(key.get(i), weight.get(i));
        }
        ItemStack wresult = getWeightedRandomItemStack(map);
        e.getInventory().setResult(wresult);
    }
    @EventHandler
    public void onBlockCook(BlockCookEvent e) {
        ItemStack result = e.getResult();
        ItemStack source = e.getSource();
        StarVerseRecipe svrsource = RecipeManager.bukkitresultToSVRecipe(result);
        if (svrsource != null) {
            if (Objects.equals(svrsource.getIngredients().get(0).getItemMeta(), source.getItemMeta())) {
                Map<ItemStack, Double> map = new HashMap<>();
                List<ItemStack> key = svrsource.getResultItems();
                List<Double> weight = svrsource.getWeight();
                for (int i=0; i<key.size(); i++) {
                    map.put(key.get(i), weight.get(i));
                }
                ItemStack wresult = getWeightedRandomItemStack(map);
                e.setResult(wresult);
            } else {
                e.setCancelled(true);
            }
        }
    }
}