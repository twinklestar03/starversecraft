package dev.twinklestar03.starverse.starversecraft.recipes;

import org.bukkit.inventory.BlastingRecipe;
import org.bukkit.inventory.Recipe;

public class RecipeBlasting extends StarVerseRecipe {

    public RecipeBlasting(RecipeType recipeType, String id, float exp, int time) {
        super(recipeType, id, exp, time);
    }

    public Recipe toBukkitRecipe() {
        BlastingRecipe blastingRecipe = new BlastingRecipe(
            this.bukkitId,
            this.resultItems.get(0),
            this.ingredients.get(0).getType(),
            this.experience,
            this.cooktime
        );

        return blastingRecipe;
    }
}