package dev.twinklestar03.starverse.starversecraft.recipes;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.SmokingRecipe;

import java.util.List;

public class RecipeSmoking extends StarVerseRecipe {
    public RecipeSmoking(RecipeType recipeType, String id, float exp, int time) {
        super(recipeType, id, exp, time);
    }


    public Recipe toBukkitRecipe() {
        SmokingRecipe smokingRecipe = new SmokingRecipe(
            this.bukkitId,
            this.resultItems.get(0),
            this.ingredients.get(0).getType(),
            this.experience,
            this.cooktime
        );

        return smokingRecipe;
    }
}