package dev.twinklestar03.starverse.starversecraft.recipes;

import dev.twinklestar03.starverse.starversecraft.StarVerseCraft;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.ArrayList;
import java.util.List;

public abstract class StarVerseRecipe {
    protected String id;
    protected NamespacedKey bukkitId;
    protected RecipeType recipeType = RecipeType.NONE;
    protected float experience = 1.0F;
    protected int cooktime = 1;
    protected Material source = Material.ANVIL;

    protected List<ItemStack> resultItems = new ArrayList<>();
    protected List<Double> resultWeights = new ArrayList<>();

    protected List<ItemStack> ingredients = new ArrayList<ItemStack>() {
        private static final long serialVersionUID = 1L;

        {
            for (int i = 0; i < 9; i++)
                add(new ItemStack(Material.AIR));
        }
    };

    public StarVerseRecipe(RecipeType recipeType, String id) {
        this.recipeType = recipeType;
        this.id = id;
        this.bukkitId = new NamespacedKey(StarVerseCraft.plugin, id);
    }

    public StarVerseRecipe(RecipeType recipeType, String id, float exp, int time) {
        this.recipeType = recipeType;
        this.id = id;
        this.experience = exp;
        this.cooktime = time;
        this.bukkitId = new NamespacedKey(StarVerseCraft.plugin, id);
    }

    public void setIngredient(Integer slot, ItemStack item) {
        this.ingredients.set(slot, item);
    }

    public void setIngredients(List<ItemStack> ingredients) {
        this.ingredients = ingredients;
    }

    public void setExperience(float exp) {
        this.experience = exp;
    }

    public void setCooktime(int time) {
        this.cooktime = time;
    }

    public List<ItemStack> getIngredients() {
        return this.ingredients;
    }

    public void addResultItem(ItemStack item) {
        this.resultItems.add(item);
        this.resultWeights.add(1.0D);
    }

    public void addResultItem(ItemStack item, Double weight) {
        this.resultItems.add(item);
        this.resultWeights.add(weight);
    }

    public void removeResultItem(ItemStack item) {
        this.resultWeights.remove(this.resultItems.indexOf(item));
        this.resultItems.remove(item);
    }

    public List<ItemStack> getResultItems() {
        return this.resultItems;
    }

    public void setRecipeType(RecipeType recipeType) {
        this.recipeType = recipeType;
    }

    public RecipeType getRecipeType() {
        return this.recipeType;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setBukkitId(NamespacedKey id) {
        this.bukkitId = id;
    }

    public NamespacedKey getBukkitId() {
        return this.bukkitId;
    }

    public Boolean setWeight(ItemStack item, Double weight) {
        int idx = this.resultItems.indexOf(item);
        if (idx == -1)
            return false;
        this.resultWeights.set(idx, weight);
        return true;
    }


    public void setWeights(List<Double> weights) {
        this.resultWeights = weights;
    }

    public List<Double> getWeight() {
        return this.resultWeights;
    }

    public abstract Recipe toBukkitRecipe();
}
