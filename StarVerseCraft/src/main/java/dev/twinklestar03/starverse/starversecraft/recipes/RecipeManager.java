package dev.twinklestar03.starverse.starversecraft.recipes;

import dev.twinklestar03.starverse.starversecraft.StarVerseCraft;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RecipeManager {
    private static Map<String, StarVerseRecipe> loadedRecipes = new HashMap<>();
    private static Map<ItemStack, StarVerseRecipe> resultToSVRecipe = new HashMap<>();

    public static void registerRecipe(StarVerseRecipe SVRecipe) {
        Recipe bukkitRecipe = SVRecipe.toBukkitRecipe();
        StarVerseCraft.plugin.getServer().addRecipe(bukkitRecipe);
        resultToSVRecipe.put(bukkitRecipe.getResult(), SVRecipe);
        loadedRecipes.put(SVRecipe.getId(), SVRecipe);
    }

    public static void unregisterRecipe(StarVerseRecipe recipe) {
        StarVerseRecipe oldRecipe = loadedRecipes.get(recipe.getId());
        if (oldRecipe == null) {
            return;
        }
        resultToSVRecipe.remove(oldRecipe.toBukkitRecipe().getResult());
        loadedRecipes.remove(oldRecipe.getId());

        StarVerseCraft.plugin.getServer().removeRecipe(
            oldRecipe.getBukkitId()
        );
    }

    /**
     * Parse bukkitRecipe into StarVerseRecipe
     * 
     * @return StarVerseRecipe, null if there's no such StarVerseRecipe
     */
    public static StarVerseRecipe bukkitRecipeToSVRecipe(Recipe bukkitRecipe) {
        if (!resultToSVRecipe.containsKey(bukkitRecipe.getResult()))
            return null;
        
        return resultToSVRecipe.get(bukkitRecipe.getResult());
    }

    public static StarVerseRecipe bukkitresultToSVRecipe(ItemStack result) {
        if (!resultToSVRecipe.containsKey(result))
            return null;

        return resultToSVRecipe.get(result);
    }

    public static StarVerseRecipe newRecipe(RecipeType recipeType, String id, List<ItemStack> results, List<Double> weights, List<ItemStack> ingredients) {
        StarVerseRecipe svrecipe;
        switch (recipeType) {
            case SHAPED:
                svrecipe = new RecipeShaped(recipeType, id);
                break;
            case SHAPELESS:
                svrecipe = new RecipeShapeless(recipeType, id);
                break;
            case STONECUTTING:
                svrecipe = new RecipeStonecutting(recipeType, id);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + recipeType);
        }

        svrecipe.setIngredients(ingredients);
        for (int i = 0; i < results.size(); i++) {
            svrecipe.addResultItem(results.get(i), weights.get(i));
        }

        return svrecipe;
    }

    public static StarVerseRecipe newRecipe(RecipeType recipeType, String id, List<ItemStack> results, List<Double> weights, List<ItemStack> ingredients, float exp, int time) {
        StarVerseRecipe svrecipe;
        switch (recipeType) {
            case CAMPFIRE:
                svrecipe = new RecipeCampfire(recipeType, id, exp, time);
                break;
            case FURNACE:
                svrecipe = new RecipeFurnace(recipeType, id, exp, time);
                break;
            case BLASTING:
                svrecipe = new RecipeBlasting(recipeType, id, exp, time);
                break;
            case SMOKEING:
                svrecipe = new RecipeSmoking(recipeType, id, exp, time);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + recipeType);
        }

        svrecipe.setIngredients(ingredients);
        for (int i = 0; i < results.size(); i++) {
            svrecipe.addResultItem(results.get(i), weights.get(i));
        }

        return svrecipe;
    }


    public static void saveRecipe(StarVerseRecipe recipe) {
        YamlConfiguration yamlConfiguration = new YamlConfiguration();

        yamlConfiguration.set("id", recipe.getId());
        yamlConfiguration.set("recipe_type", recipe.getRecipeType());
        yamlConfiguration.set("results", recipe.getResultItems());
        yamlConfiguration.set("weights", recipe.getWeight());
        yamlConfiguration.set("ingredients", recipe.getIngredients());

        StarVerseCraft.saveConfig("/recipes/" + recipe
            .getRecipeType().toString(), recipe
            .getId() + ".yml", yamlConfiguration);
    }

    public static void saveAllRecipe() {
        for (Map.Entry<String, StarVerseRecipe> entry : loadedRecipes.entrySet()) {
            saveRecipe(entry.getValue());
        }
    }

    public static StarVerseRecipe getStarVerseRecipe(String id) {
        if (loadedRecipes.containsKey(id)) {
            return loadedRecipes.get(id);
        }
        return null;
    }

    public static List<String> getIds() {
        List<String> ids = new ArrayList<>();
        for (Map.Entry<String, StarVerseRecipe> entry : loadedRecipes.entrySet()) {
            ids.add(entry.getKey());
        }
        return ids;
    }
}


