package dev.twinklestar03.starverse.starversecraft.recipes;

public enum RecipeType {
    BLASTING,
    CAMPFIRE,
    FURNACE,
    SHAPED,
    SHAPELESS,
    SMITHING,
    SMOKEING,
    STONECUTTING,
    NONE
}

