package dev.twinklestar03.starverse.starversecraft.recipes;

import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Recipe;

public class RecipeFurnace extends StarVerseRecipe {

    public RecipeFurnace(RecipeType recipeType, String id, float exp, int time) {
        super(recipeType, id, exp, time);
    }

    public Recipe toBukkitRecipe() {
        FurnaceRecipe furnaceRecipe = new FurnaceRecipe(
            this.bukkitId,
            this.resultItems.get(0),
            this.ingredients.get(0).getType(),
            this.experience,
            this.cooktime
        );

        return furnaceRecipe;
    }
}
