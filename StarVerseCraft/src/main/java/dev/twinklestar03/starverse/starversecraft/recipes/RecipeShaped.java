package dev.twinklestar03.starverse.starversecraft.recipes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import dev.twinklestar03.starverse.starversecraft.StarVerseCraft;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import org.bukkit.inventory.meta.ItemMeta;


public class RecipeShaped extends StarVerseRecipe {

    public RecipeShaped(RecipeType recipeType, String id) {
        super(recipeType, id);
    }

    public Recipe toBukkitRecipe() {
        ItemStack showResult = new ItemStack(Material.PAPER);
        List<String> resultLores = new ArrayList<>();

        showResult.getItemMeta().setDisplayName(ChatColor.AQUA + "[StarVerseCraft]");

        // We could read this string from config.
        resultLores.add(ChatColor.GOLD + "此配方可以製造的道具 > ");
        for (ItemStack item : this.resultItems) {
            if (item.getItemMeta().hasDisplayName()) {
                resultLores.add(ChatColor.RESET + item.getItemMeta().getDisplayName());
                continue;
            }
            if (item.getItemMeta().hasLocalizedName()) {
                resultLores.add(ChatColor.RESET + item.getItemMeta().getLocalizedName());
                continue;
            }
            resultLores.add(ChatColor.RESET + item.getType().toString());
        }
        ItemMeta shresult = showResult.getItemMeta();
        shresult.setLore(resultLores);
        showResult.setItemMeta(shresult);

        // Add a NBTTag on it to prevent result confilct.
        net.minecraft.server.v1_15_R1.ItemStack nmsShowResult = CraftItemStack.asNMSCopy(showResult);
        NBTTagCompound idCompound = (nmsShowResult.hasTag()) ? nmsShowResult.getTag() : new NBTTagCompound();
        idCompound.setString("svc.id", this.id);
        nmsShowResult.setTag(idCompound);

        showResult = CraftItemStack.asBukkitCopy(nmsShowResult);

        ShapedRecipe shapedRecipe = new ShapedRecipe(
            this.bukkitId,
            showResult
        );
        shapedRecipe.shape("abc", "def", "ghi");

        String slotStr = "abcdefghi";
        for (int i = 0; i < 9; i++) {
            if (this.ingredients.get(i) != null) {
                shapedRecipe.setIngredient(
                    slotStr.charAt(i),
                    this.ingredients.get(i).getType()
                );
            }
        }

        return shapedRecipe;

    }
}

