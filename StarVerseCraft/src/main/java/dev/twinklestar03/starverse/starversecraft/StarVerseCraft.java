package dev.twinklestar03.starverse.starversecraft;

import dev.twinklestar03.starverse.starversecraft.commands.StarVerseCraftCommand;
import dev.twinklestar03.starverse.starversecraft.events.ItemCraftingEvent;
import dev.twinklestar03.starverse.starversecraft.events.PreprocessEvents;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class StarVerseCraft extends JavaPlugin {
    public static Logger logger;
    public static StarVerseCraft plugin;
    public static FileConfiguration StarVerseCraftCfg;

    private ConsoleCommandSender console = getServer().getConsoleSender();

    public void onLoad() {
        logger = getLogger();
        getDataFolder().mkdirs();
    }

    public void onEnable() {
        plugin = this;
        StarVerseCraftCfg = loadConfig("", "config.yml", Boolean.TRUE);

        if (!StarVerseCraftCfg.getBoolean("enable")) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        registerCommands();
        registerEvents();
        registerPerms();

        console.sendMessage(ChatColor.GREEN + "\n");
        console.sendMessage(ChatColor.GREEN + "   ______          _   __                _____         _____   ");
        console.sendMessage(ChatColor.GREEN + "  / __/ /____ ____| | / /__ _______ ___ / ___/______ _/ _/ /_  ");
        console.sendMessage(ChatColor.GREEN + " _\\ \\/ __/ _ `/ __/ |/ / -_) __(_-</ -_) /__/ __/ _ `/ _/ __/");
        console.sendMessage(ChatColor.GREEN + "/___/\\__/\\_,_/_/  |___/\\__/_/ /___/\\__/\\___/_/  \\_,_/_/ \\__/");
        console.sendMessage(ChatColor.GREEN + "\n");
        console.sendMessage(ChatColor.GREEN + "[+] StarVerseCraft is now enabled.");
    }


    public void onDisable() {
        console.sendMessage(ChatColor.GREEN + "\n");
        console.sendMessage(ChatColor.GREEN + "   ______          _   __                _____         _____   ");
        console.sendMessage(ChatColor.GREEN + "  / __/ /____ ____| | / /__ _______ ___ / ___/______ _/ _/ /_  ");
        console.sendMessage(ChatColor.GREEN + " _\\ \\/ __/ _ `/ __/ |/ / -_) __(_-</ -_) /__/ __/ _ `/ _/ __/");
        console.sendMessage(ChatColor.GREEN + "/___/\\__/\\_,_/_/  |___/\\__/_/ /___/\\__/\\___/_/  \\_,_/_/ \\__/");
        console.sendMessage(ChatColor.GREEN + "\n");
        console.sendMessage(ChatColor.GREEN + "[-] StarVerseCraft is now disabled.");
        console.sendMessage(ChatColor.GREEN + "[-] QAQ.");
    }

    private void registerCommands() {
        getCommand("svc").setExecutor(new StarVerseCraftCommand(this));
    }


    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new PreprocessEvents(), this);
        getServer().getPluginManager().registerEvents(new ItemCraftingEvent(), this);
    }


    private void registerPerms() {
    }


    public static FileConfiguration loadConfig(String cfgPath, String fileName, Boolean hasDefault) {
        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + cfgPath, fileName));

        if (hasDefault) {
            FileConfiguration defaultConfig = null;
            Reader defaultConfigStream = new InputStreamReader(plugin.getResource(fileName), StandardCharsets.UTF_8);
            YamlConfiguration yamlConfiguration1 = YamlConfiguration.loadConfiguration(defaultConfigStream);
            yamlConfiguration.setDefaults(yamlConfiguration1);
            yamlConfiguration.options().copyDefaults(true);

            saveConfig(cfgPath, fileName, yamlConfiguration1);
        }

        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + cfgPath, fileName));
    }

    public static void saveConfig(String cfgPath, String fileName, FileConfiguration config) {
        try {
            File dest = new File(plugin.getDataFolder() + cfgPath, fileName);
            dest.getParentFile().mkdirs();
            Writer fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dest), StandardCharsets.UTF_8));
            fileWriter.write(config.saveToString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
