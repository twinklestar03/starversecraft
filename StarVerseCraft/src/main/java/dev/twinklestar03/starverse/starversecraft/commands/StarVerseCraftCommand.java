package dev.twinklestar03.starverse.starversecraft.commands;

import dev.twinklestar03.starverse.starversecraft.StarVerseCraft;
import dev.twinklestar03.starverse.starversecraft.recipes.RecipeManager;
import dev.twinklestar03.starverse.starversecraft.recipes.RecipeShaped;
import dev.twinklestar03.starverse.starversecraft.recipes.RecipeType;
import dev.twinklestar03.starverse.starversecraft.recipes.StarVerseRecipe;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class StarVerseCraftCommand
    implements CommandExecutor {
    private StarVerseCraft plugin;

    public StarVerseCraftCommand(StarVerseCraft plugin) {
        this.plugin = plugin;
    }


    public boolean onCommand(CommandSender sender, Command command, String label, String[] argv) {
        Player player = (Player) sender;

        if (argv.length < 2) {
            player.spigot().sendMessage((new ComponentBuilder("Wrong Command Usage!"))
                .color(ChatColor.RED)
                .append("/svc <RecipeType> <Action> [Args]").color(ChatColor.GOLD)
                .create());

            return true;
        }
        if (argv[0].equalsIgnoreCase("stonecuttingRecipe")) {
            if (argv[1].equalsIgnoreCase("test")) {
                List<ItemStack> results = new ArrayList<>();
                List<ItemStack> ingredients = new ArrayList<>();
                List<Double> weights = new ArrayList<>();
                weights.add(0.5D);
                weights.add(1.0D);

                ingredients.add(player.getInventory().getItemInOffHand());
                results.add(player.getInventory().getItemInMainHand());
                results.add(new ItemStack(Material.BELL));
                StarVerseRecipe recipe = RecipeManager.newRecipe(RecipeType.STONECUTTING, "test", results, weights, ingredients);
                RecipeManager.registerRecipe(recipe);
                RecipeManager.saveAllRecipe();
                return true;
            }
        }
        if (argv[0].equalsIgnoreCase("SmokeingRecipe")) {
            if (argv[1].equalsIgnoreCase("test")) {
                List<ItemStack> results = new ArrayList<>();
                List<ItemStack> ingredients = new ArrayList<>();
                List<Double> weights = new ArrayList<>();
                weights.add(0.5D);
                weights.add(1.0D);

                ingredients.add(player.getInventory().getItemInOffHand());
                results.add(player.getInventory().getItemInMainHand());
                results.add(new ItemStack(Material.BELL));
                StarVerseRecipe recipe = RecipeManager.newRecipe(RecipeType.SMOKEING, "test", results, weights, ingredients, 1, 2);
                RecipeManager.registerRecipe(recipe);
                RecipeManager.saveAllRecipe();
                return true;
            }
        }
        if (argv[0].equalsIgnoreCase("BlastingRecipe")) {
            if (argv[1].equalsIgnoreCase("test")) {
                List<ItemStack> results = new ArrayList<>();
                List<ItemStack> ingredients = new ArrayList<>();
                List<Double> weights = new ArrayList<>();
                weights.add(0.5D);
                weights.add(1.0D);

                ingredients.add(player.getInventory().getItemInOffHand());
                results.add(player.getInventory().getItemInMainHand());
                results.add(new ItemStack(Material.BELL));
                StarVerseRecipe recipe = RecipeManager.newRecipe(RecipeType.BLASTING, "test", results, weights, ingredients, 1, 2);
                RecipeManager.registerRecipe(recipe);
                RecipeManager.saveAllRecipe();
                return true;
            }
        }
        if (argv[0].equalsIgnoreCase("CampfireRecipe")) {
            if (argv[1].equalsIgnoreCase("test")) {
                List<ItemStack> results = new ArrayList<>();
                List<ItemStack> ingredients = new ArrayList<>();
                List<Double> weights = new ArrayList<>();
                weights.add(0.5D);
                weights.add(1.0D);

                ingredients.add(player.getInventory().getItemInOffHand());
                results.add(player.getInventory().getItemInMainHand());
                results.add(new ItemStack(Material.BELL));
                StarVerseRecipe recipe = RecipeManager.newRecipe(RecipeType.CAMPFIRE, "test", results, weights, ingredients, 1, 2);
                RecipeManager.registerRecipe(recipe);
                RecipeManager.saveAllRecipe();
                return true;
            }
        }
        if (argv[0].equalsIgnoreCase("FurnaceRecipe")) {
            if (argv[1].equalsIgnoreCase("test")) {
                List<ItemStack> results = new ArrayList<>();
                List<ItemStack> ingredients = new ArrayList<>();
                List<Double> weights = new ArrayList<>();
                weights.add(0.5D);
                weights.add(1.0D);

                ingredients.add(player.getInventory().getItemInOffHand());
                results.add(player.getInventory().getItemInMainHand());
                results.add(new ItemStack(Material.BELL));
                player.sendMessage(player.getInventory().getItemInMainHand().getType().toString());

                StarVerseRecipe recipe = RecipeManager.newRecipe(RecipeType.FURNACE,"test", results, weights, ingredients, 1, 2);
                RecipeManager.registerRecipe(recipe);
                RecipeManager.saveAllRecipe();
                return true;
            }
        }

        if (argv[0].equalsIgnoreCase("ShapelessRecipe")) {
            if (argv[1].equalsIgnoreCase("test")) {
                player.sendMessage("First Test: Register a recipe");
                List<ItemStack> results = new ArrayList<>();
                List<ItemStack> ingredients = new ArrayList<>();
                List<Double> weights = new ArrayList<>();
                results.add(player.getInventory().getItemInMainHand());
                results.add(new ItemStack(Material.BELL));
                weights.add(0.5D);
                weights.add(1.0D);

                ingredients.add(player.getInventory().getItemInOffHand());

                for (int i = 1; i < 9; i++) {
                    ingredients.add(null);
                }

                StarVerseRecipe recipe = RecipeManager.newRecipe(RecipeType.SHAPELESS, "test", results, weights, ingredients);
                RecipeManager.registerRecipe(recipe);
                RecipeManager.saveAllRecipe();
                return true;
            }
        }

        if (argv[0].equalsIgnoreCase("ShapedRecipe")) {
            if (argv[1].equalsIgnoreCase("test")) {
                player.sendMessage("First Test: Register a recipe");
                List<ItemStack> results = new ArrayList<>();
                List<ItemStack> ingredients = new ArrayList<>();
                List<Double> weights = new ArrayList<>();
                results.add(player.getInventory().getItemInMainHand());
                results.add(new ItemStack(Material.BELL));
                weights.add(0.5D);
                weights.add(1.0D);

                ingredients.add(player.getInventory().getItemInOffHand());

                for (int i = 1; i < 9; i++) {
                    ingredients.add(null);
                }

                StarVerseRecipe recipe = RecipeManager.newRecipe(RecipeType.SHAPED, "test", results, weights, ingredients);
                RecipeManager.registerRecipe(recipe);
                RecipeManager.saveAllRecipe();
                return true;
            }
            if (argv[1].equalsIgnoreCase("test2")) {
                player.sendMessage("Second Test: Same recipe but change ingredients");
                StarVerseRecipe recipe = RecipeManager.getStarVerseRecipe("test");
                RecipeManager.unregisterRecipe(recipe);
                recipe.setIngredient(0, player.getInventory().getItemInOffHand());
                recipe.setIngredient(1, player.getInventory().getItemInOffHand());
                recipe.setIngredient(5, player.getInventory().getItemInOffHand());

                RecipeManager.registerRecipe(recipe);
                return true;
            }
            if (argv[1].equalsIgnoreCase("all")) {
                player.spigot().sendMessage((new ComponentBuilder("All loaded recipes >"))
                    .color(ChatColor.YELLOW)
                    .create());

                for (String id : RecipeManager.getIds()) {
                    StarVerseRecipe recipe = RecipeManager.getStarVerseRecipe(id);


                    ComponentBuilder cb = (new ComponentBuilder("> ")).color(ChatColor.GOLD).append(id).color(ChatColor.GREEN).append(" (").color(ChatColor.GRAY).append(recipe.getRecipeType().toString()).color(ChatColor.AQUA).append(")").color(ChatColor.GRAY).append(" -> ").color(ChatColor.GOLD);

                    for (ItemStack item : recipe.getResultItems())
                        cb.append(Utils.makeItemComponent(item)).append(" ");
                    player.spigot().sendMessage(cb.create());
                }
                return true;
            }
        }

        return false;
    }
}
